%global distro  CentOS AutoSD
%global major   9
%global minor   0

Name:       centos-release-automotive
Version:    %{major}.%{minor}
Release:    9%{?dist}
License:    GPLv2
URL:        http://wiki.centos.org/SpecialInterestGroup/Automotive
Summary:    CentOS Automotive SIG main repo configs
Source0:    CentOS-Stream-Automotive.repo
Source1:    CentOS-Stream-Automotive-Experimental.repo

BuildArch:  noarch

Requires:   centos-release-autosd

# We still want to have the centos-stream repos and gpg keys
Requires:   centos-gpg-keys
Requires:   centos-stream-repos

%description
Configs for the CentOS Automotive SIG main package repository and release
files.

%package experimental
Summary: CentOS Automotive SIG experimental repo configs
Requires: %{name} = %{version}-%{release}

%description experimental
Configs for the CentOS Automotive SIG experimental package repository.

%prep
# Nothing to do

%install
# Install the release files

# create skeleton
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}%{_prefix}/lib

# -------------------------------------------------------------------------
# Definitions for /etc/os-release and for macros in macros.dist.  These
# macros are useful for spec files where distribution-specific identifiers
# are used to customize packages.

# Name of vendor / name of distribution. Typically used to identify where
# the binary comes from in --help or --version messages of programs.
# Examples: gdb.spec, clang.spec
%global dist_vendor CentOS
%global dist_name   %{distro}

# URL of the homepage of the distribution
# Example: gstreamer1-plugins-base.spec
%global dist_home_url https://centos.org/

# Bugzilla / bug reporting URLs shown to users.
# Examples: gcc.spec
%global dist_bug_report_url https://bugzilla.redhat.com/

# debuginfod server, as used in elfutils.spec.
%global dist_debuginfod_url https://debuginfod.centos.org/
# -------------------------------------------------------------------------

# Install the repo files
install -d %{buildroot}%{_sysconfdir}/yum.repos.d
install -p -m 644 %{SOURCE0} %{SOURCE1} %{buildroot}%{_sysconfdir}/yum.repos.d

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-Stream-Automotive.repo

%files experimental
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-Stream-Automotive-Experimental.repo


%changelog
* Wed Aug 16 2023 Michael Ho <micho@redhat.com> - 9-9
- Strip preset config files as this is provided by centos-release-autosd
- Remove gpg key file as this is provided by centos-release-autosd
- Add dependency on centos-release-autosd
- Remove conflicts/provides

* Thu May 4 2023 Eric Curtin <ecurtin@redhat.com> - 9-8
- Provide macros.dist

* Fri Mar 31 2023 Michael Ho <micho@redhat.com> - 9-7
- Provide versioned redhat-release

* Fri Mar 24 2023 Adrien Thierry <athierry@redhat.com> - 9-6
- Provide redhat-release

* Mon Mar 13 2023 Adrien Thierry <athierry@redhat.com> - 9-5
- Add preset for chronyd.service

* Wed Jan 04 2023 Michael Ho <micho@redhat.com> - 9-4
- Add default .preset files

* Fri Oct 14 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-3
- Mark the repos to be skipped if unavailable

* Thu Oct 13 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-2
- Add a dependency to centos-stream-repos and centos-gpg-keys

* Tue Sep 06 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-1
- Initial package for CentOS Stream 9
